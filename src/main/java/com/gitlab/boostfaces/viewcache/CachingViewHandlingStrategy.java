/*
 * Copyright 2019 BoostFaces
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.boostfaces.viewcache;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewDeclarationLanguage;
import javax.faces.view.ViewDeclarationLanguageWrapper;

import com.gitlab.boostfaces.Constants;
import com.sun.faces.renderkit.RenderKitUtils.PredefinedPostbackParameter;

public class CachingViewHandlingStrategy extends ViewDeclarationLanguageWrapper {

	@Deprecated
	public CachingViewHandlingStrategy() {
	}

	public CachingViewHandlingStrategy(final ViewDeclarationLanguage wrapped) {
		super(wrapped);
	}

	@Override
	public void buildView(final FacesContext context, final UIViewRoot root) throws IOException {
		super.buildView(context, root);
	}

	@Override
	public void renderView(final FacesContext context, final UIViewRoot view) throws IOException {
		super.renderView(context, view);
		final String newViewStateId = getNewViewStateId(context);
		final String requestedViewStateId = getRequestedViewStateId(context);
		if (!Objects.equals(newViewStateId, requestedViewStateId)) {
			putViewToCache(context, view.getViewId(), view);
		}
	}

	@Override
	public UIViewRoot createView(final FacesContext context, final String viewId) {
		final UIViewRoot viewRoot = super.createView(context, viewId);
		return viewRoot;
	}

	@Override
	public UIViewRoot restoreView(final FacesContext context, final String viewId) {
		UIViewRoot result = getViewFromCache(context, viewId);
		if (null == result) {
			result = super.restoreView(context, viewId);
		}
		return result;
	}

	/**
	 * Gets the view cache from the current user session.
	 * 
	 * @param context
	 * @param viewId
	 * @return
	 */
	private Map<CacheKey, CacheEntry> getCache(final FacesContext context, final String viewId) {
		final Map<String, Object> sessionMap = context.getExternalContext().getSessionMap();
		@SuppressWarnings("unchecked")
		final Map<CacheKey, CacheEntry> result = (Map<CacheKey, CacheEntry>) sessionMap.computeIfAbsent(
				Constants.VIEW_CACHE_SESSION_KEY, (k) -> new ConcurrentHashMap<CacheKey, CacheEntry>());
		return result;
	}

	private void putViewToCache(final FacesContext context, final String viewId, final UIViewRoot viewRoot) {
		final Map<CacheKey, CacheEntry> cache = getCache(context, viewId);
		final String newViewStateId = getNewViewStateId(context);
		final CacheKey key = createCacheKey(context, viewId, newViewStateId);
		final CacheEntry cacheEntry = createCacheEntry(context, viewId, viewRoot);
		cache.put(key, cacheEntry);
	}

	private UIViewRoot getViewFromCache(final FacesContext context, final String viewId) {
		final Map<CacheKey, CacheEntry> cache = getCache(context, viewId);
		final String requestedViewStateId = getRequestedViewStateId(context);
		final CacheKey key = createCacheKey(context, viewId, requestedViewStateId);
		final CacheEntry cacheEntry = cache.get(key);
		UIViewRoot result = null;
		if (null != cacheEntry) {
			result = cacheEntry.getViewRoot();
		}
		return result;
	}

	private CacheKey createCacheKey(final FacesContext context, final String viewId, final String viewStateId) {
		final CacheKey result = new CacheKey(viewId, viewStateId);
		return result;
	}

	private String getNewViewStateId(final FacesContext context) {
		final String newViewStateId = (String) context.getAttributes().get("com.sun.faces.ViewStateValue");
		return newViewStateId;
	}

	private String getRequestedViewStateId(final FacesContext context) {
		final String requestedViewStateId = PredefinedPostbackParameter.VIEW_STATE_PARAM.getValue(context);
		return requestedViewStateId;
	}

	private CacheEntry createCacheEntry(final FacesContext context, final String viewId, final UIViewRoot viewRoot) {
		final CacheEntry result = new CacheEntry(viewId, viewRoot);
		return result;
	}
}
