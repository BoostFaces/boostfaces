/*
 * Copyright 2019 BoostFaces
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.boostfaces.viewcache;

import javax.faces.view.ViewDeclarationLanguage;
import javax.faces.view.ViewDeclarationLanguageFactory;

public class CachingViewDeclarationLanguageFactory extends ViewDeclarationLanguageFactory {

	@Deprecated
	public CachingViewDeclarationLanguageFactory() {
	}

	public CachingViewDeclarationLanguageFactory(final ViewDeclarationLanguageFactory wrapped) {
		super(wrapped);
	}

	@Override
	public ViewDeclarationLanguage getViewDeclarationLanguage(final String viewId) {
		final ViewDeclarationLanguage vdl = getWrapped().getViewDeclarationLanguage(viewId);
		final ViewDeclarationLanguage result;
		if (isCachingEnabled()) {
			result = new CachingViewHandlingStrategy(vdl);
		} else {
			result = vdl;
		}
		return result;
	}

	private boolean isCachingEnabled() {
		// TODO add configurability to enable caching based on INIT PARAM and/or viewId
		// and/or advised by a ServletFilter
		return true;
	}

}
