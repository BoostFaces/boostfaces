/*
 * Copyright 2019 BoostFaces
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.boostfaces.viewcache;

import java.io.Serializable;

public class CacheKey implements Serializable {
	private static final long serialVersionUID = 2440785161549634918L;

	private final String viewId;

	private final String viewStateID;

	public CacheKey(final String viewId, final String viewStateID) {
		super();
		this.viewId = viewId;
		this.viewStateID = viewStateID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((viewId == null) ? 0 : viewId.hashCode());
		result = prime * result + ((viewStateID == null) ? 0 : viewStateID.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CacheKey other = (CacheKey) obj;
		if (viewId == null) {
			if (other.viewId != null)
				return false;
		} else if (!viewId.equals(other.viewId))
			return false;
		if (viewStateID == null) {
			if (other.viewStateID != null)
				return false;
		} else if (!viewStateID.equals(other.viewStateID))
			return false;
		return true;
	}

}
