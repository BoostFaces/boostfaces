/*
 * Copyright 2019 BoostFaces
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.boostfaces.viewcache;

import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.time.Instant;

import javax.faces.component.UIViewRoot;

public class CacheEntry implements Serializable {

	private static final long serialVersionUID = -4916920933317716187L;

	private final Instant creationTime = Instant.now();

	/**
	 * Arbitrary reference to viewRoot.
	 */
	private transient Reference<UIViewRoot> viewRootRef;

	/**
	 * The view ID
	 */
	private final String viewId;

	/**
	 * Strong reference to viewRoot.
	 */
	private transient UIViewRoot viewRoot;

	public CacheEntry(final String viewId, final UIViewRoot viewRoot) {
		super();
		this.viewId = viewId;
		this.viewRoot = viewRoot;
		viewRootRef = new SoftReference<UIViewRoot>(viewRoot);
	}

	/**
	 * Gets the cached {@link UIViewRoot} or null if invalid.
	 * 
	 * @return cached {@link UIViewRoot} or null if invalid.
	 */
	public UIViewRoot getViewRoot() {
		UIViewRoot result = viewRoot;
		if (null == result && null != viewRootRef) {
			result = viewRootRef.get();
		}
		return viewRoot;
	}

	/**
	 * Gets the {@link Instant} when this cacheEntry was created.
	 * 
	 * @return {@link Instant} when this cacheEntry was created.
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Gets the view Id.
	 * 
	 * @return the view Id.
	 */
	public String getViewId() {
		return viewId;
	}
}
